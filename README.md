runmeurl: http://m.marschall.gitlab.io/ap2021/

# AP2021 - HOW TO GUIDE!
----
## Setup repository (only if you haven't already)
First, login to https://gitlab.com/

You should see a page that looks like this:

<img src="./tutimages/1.png" width=70%>

Now we need to setup a repository for this course, so you need to press the "New project" button.



After that you have to select "Create blank project":

<img src="./tutimages/3.png" width=70%>

For the Project name write "AP2021", and make shure that the "Visibility level" is set to "Public"

<img src="./tutimages/4.png" width=70%>


Now we have our repository! :)

<img src="./tutimages/5.png" width=70%>

---
## Setup webserver (so that you can run your projects) + upload files

We need to click to open the "Web IDE", which enable us to upload and create files and folders.

<img src="./tutimages/16.png" width=70%>

Thereafter we need to add a "New file"

<img src="./tutimages/17.png" width=70%>

we need to name the file:
```yml
.gitlab-cl.yml
```
This file allows us to run scripts and initialize our web server, however, you do not need to remember that ;)

<img src="./tutimages/18.png" width=70%>

in the ```.gitlab-cl.yml```  file, we need to add the following code:

```yml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```


<img src="./tutimages/19.png" width=70%>

Afterwards, we can create a "New directory" and name it "public" (note: it is important to remember that this is Case sensitive, which mean that you should not name it "Public" or "PUBLIC")

<img src="./tutimages/20.png" width=70%>

<img src="./tutimages/21.png" width=70%>

Now we need to press the "Commit" button to save the changes to our project.

<img src="./tutimages/22.png" width=70%>

Here it is important to "Commit to master branch", and then press "Commit"

<img src="./tutimages/23.png" width=70%>

## Setup Git to upload files via terminal
- Download git and install:
https://git-scm.com/downloads

<img src="./tutimages/12.png" width=70%>

- Then we need to install a package for the terminal that we are going to use:

<img src="./tutimages/14.png" width=70%>

## Create local copy of GIT repository
9
10
11_1

Now we need to set up a local copy of our git repository, so that we can start syncing between our own comuter and the server via GIT.

Start out by creating a new folder somewhere on your computer. If you are syncing you files with Dropbox, OneDrive or any other service, then you will need to disable sync for that folder, or make sure that the folders are always kept on your computer's harddrive.

<img src="./tutimages/9.png" width=70%>

Open atom and press "Add Preject Folder" or "Add folder" and then navigate to the folder you have just created.

<img src="./tutimages/10.png" width=70%>

<img src="./tutimages/11_1.png" width=70%>


Now go to your gitlab project and press the blue "Clone" button and copy the link under "Clone with HTTPS"

<img src="./tutimages/25.png" width=70%>

Now in Atom you have to open up a new terminal by doing the following:

<img src="./tutimages/24.png" width=70%>

It will show up at the buttom of the window and you have to type in the following command:

```GIT
git clone https://gitlab.com/(Your account)/(repository).git
```
<img src="./tutimages/26.png" width=70%>

when it is finished downloading, we can open the folder by typing:

```
cd ap2021
```
Now we have a functioning git library!

We have to tell who we are to be able to post changes to our repository. To do this you have to fill write these next lines in the terminal, one by one. Replace email and name with own information.

```
git config --global user.email "you@example.com"
```
```
git config --global user.name "Your Name"
```
Now we should be ready. The first time that you try to upload somthing, you will have to provide username and password to your Gitlab account. Let's go trough the process of uploading:

Let's say that we have added som files or edited some, then we can add all changes by typing the following:
```
git add .
```

Then to we need to "Commit" these changes and type in a message og what we have added (you do not need to write anything special).

```
git commit -m "i have added my miniEx 3"
```

And now we can finally upload it to Gitlab:

```
git push origin master
```

Now everything should have been uploaded to GitLab.

## Small GIT cheet sheet
---

```
git pull
```
Download files from you gitlab repository to make sure that you have all files on your local computer.

---
```
git add .
```
Add all files to be uploaded

---
```
git commit -m "(some message)"
```
We need to "Commit" changes before we can push them to gitlab. It is a way of keeping track when working multiple people or just on larger projects.

---
```
git push origin master
```

Uploads all local branch commits to GitHub

---


for more commands check out this:
https://education.github.com/git-cheat-sheet-education.pdf
