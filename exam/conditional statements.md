___
___
___
# Conditional statements and structure
____
___
____
</br>
</br>
</br>

If the boolean expression inside the if() is true then it will execute that block of code.
```JavaScript
if(tired){
  takeNap();
}
else if (stillTired) {
  drinkCoffe();
}
else {
  takeADayOf();
}

````
</br>
</br>
</br>

---

</br>
</br>
</br>

### Using conditions allow you to set a decision path of the program

</br>
</br>
</br>

---

</br>
</br>
</br>

### You can add more conditions to an if() and else if() using logical operators

```JavaScript
if(true && false || !false){}
```

Relational Operators:
```JavaScript
/*
>   Greater than
<   Less than
>=  greater than or equal to
<=  less than or equal to
==  equality
=== equality (include strict data type checking)
!=  not equal to
!== inequality with strict type checking
*/
```
(_Winnie Soon:_ https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/2-VariableGeometry)

</br></br></br>
____
</br></br></br>

### The conditional structure plays an essential part in the politics of the software

```JavaScript
if (gender.localeCompare("Female") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
  for (let t = 0; t < jobs[0].jobsforwomen.length; t++) {//Goin through element in array
    let posY;
    posY = 50 * t + 400;//Increments the y position of the text
    text(jobs[0].jobsforwomen[t], 1100, posY);//Display the job
  }
}

if (gender.localeCompare("Male") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
  for (let t = 0; t < jobs[0].jobsformen.length; t++) {//Going through each element in array
    let posY;
    posY = 50 * t + 400;//Increments the y position of the text
    text(jobs[0].jobsformen[t], 1100, posY);//Display the job
  }
}
````

```JavaScript
if (numberOfMale > numberOfFemale) {
  console.log('Male');
  return "Male";
}
if (numberOfMale < numberOfFemale) {
  console.log('Female');
  layout.genderChange.show();
  return "Female";
}
```

</br></br></br>


___
</br></br></br>


### Deterministic structure

</br></br></br>
___

# //Continue with Algorithms
