</br></br></br>
___


# The politics of our Final Project
## The Project
https://roeskva.gitlab.io/ap2020/Final%20Project
![](assets/markdown-img-paste-20200609125610389.png)
![](assets/markdown-img-paste-20200609125639638.png)
___
</br></br></br>
## Politics within:
 - ### Concept
 - ### creation
 - ### execution + writing

</br></br></br>
____
</br></br></br>

## Concept
![](assets/markdown-img-paste-20200609195832242.png)

____
</br></br></br>

## Creation:

</br></br></br>

____
</br></br></br>

### Lack of diversity
### We asked about gender
![](assets/markdown-img-paste-20200609201033994.png)

</br></br></br>
____
</br></br></br>

## Writing + Execution:

</br></br></br>
____

</br></br></br>

### Conditional Structure + Language
```JavaScript
if (numberOfMale > numberOfFemale) {
  console.log('Male');
  return "Male";
}
if (numberOfMale < numberOfFemale) {
  console.log('Female');
  layout.genderChange.show();
  return "Female";
}
```
```//We  don't even bother to use an else { }```
```//numberOfMale //numberOfFemale == commodification ```


```JavaScript
function displaySalary(gender) {
  fill(0);
  textSize(38);

  if (gender.localeCompare("Female") == 0) {
    text(jobs[1].salarywomen[0] + " DKK", 1090, 250);
  }

  if (gender.localeCompare("Male") == 0) {
    text(jobs[1].salarymen[0] + " DKK", 1090, 250);
  }
}
```


```//salarymen //salarywomen```

</br></br></br>
____
</br></br></br>


### The relations of the classification algorithm
  - #### Inner Relations = the relation within the software

```JavaScript
function classifyGender() {
  captureFlipped = ml5.flipImage(capture); //Flipping the images input from webcam
  classifier.classify(captureFlipped, handleGender);//Classifing the captureFlipped according to our model and call handleGender and passing the result or error as an argument
  captureFlipped.remove();//Remove the stored image to allow for another image to be assigned
}

function handleGender(error, results) {
  if (error) {
    console.error(error);//if anything is assign to the error argument then display it in the console
    return;
  }


  label = results[0].label //The "result" argument parsed from classifier.classify() is an array with the most proable label at index 0
  arrayOfResults.push(label);//We add this label to an array
  console.log(label);//prints the label to the console
  counter++;
  if (counter > 10) {
    clearInterval(interval);//Stops the interval from looping
    userGender = determineGender();//assigning the reurned value from determineGender();
  }
}
```
//disregarding doubt expressed in result[]

```JavaScript
function determineGender() {
  imageBeingTaken = false;
  capture.show();//Show the webcam feed again

  let numberOfMale = 0;
  let numberOfFemale = 0;
  for (let i = 0; i < arrayOfResults.length; i++) {
    if (arrayOfResults[i].localeCompare("Female") == 0) {
      numberOfFemale++;
    }
    if (arrayOfResults[i].localeCompare("Male") == 0) {
      numberOfMale++;
    }
  }
  console.log(numberOfMale);
  console.log(numberOfFemale);

  if (numberOfMale > numberOfFemale) {
    console.log('Male');
    return "Male";
  }
  if (numberOfMale < numberOfFemale) {
    console.log('Female');
    layout.genderChange.show();
    return "Female";
  }
}
```
</br></br></br>

____
</br></br></br>

  - #### Outer Relations = the relation to the context

![](assets/markdown-img-paste-20200609201305468.png)

//training data - this relation might be static in our case.

//data input

//Making of people

//Labeling

//Language

#### The computational structure might be deterministic but the effect of the software is not.

</br></br></br>
___

### Governmentality
#### Conducting the conduct

![](assets/markdown-img-paste-20200609141540896.png)

//The button

//if the writing was links

</br></br></br>
____
</br></br></br>

#### Within the system

![](assets/markdown-img-paste-20200609131243251.png)

</br></br></br>
____
</br></br></br>


#### Outside the system

![](assets/markdown-img-paste-20200609143011328.png)

</br></br></br>
____
