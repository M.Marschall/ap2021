# Object abstraction

 - ### Objects
   - Defining and Object
   - Interaction between objects
 - ### Object oriented programmering
 - ### Abstraction of reality
____
</br></br></br>

## Objects

Groupings of data and methods

```Javascript
let mads = {
  "hairColor": "brown",

  "sayHello": function () {
    console.log('hello!');
  }
}

```
</br></br></br>
____
</br></br></br>

### Abstract Models of Reality
![](assets/markdown-img-paste-20200606095041104.png)


![](assets/markdown-img-paste-20200606094601241.png)
</br>
</br></br></br>
_____
</br></br></br>

#### <b>Abstraction is always subjective and selective</b>

//_JSON downloaded from facebook - not a javascript object, but an JSON object;_

Mads Marschall:
```JSON
{
  "friend_peer_group": "Starting Adult Life"
}
```

Kenneth Marschall:
```JSON
{
  "friend_peer_group": "Etableret voksenliv"
}
```
_Note: These are also abstract classifications._

</br></br></br>
____

</br></br></br>

Even though execution is a linear process from, objects allow for a dynamic event based reordering. They can create the sense of operating in executing in multiple threads making them more dynamic.




```JavaScript
function draw() {
  for (let i = 0; i < arrayOfParticles.length; i++) {
    arrayOfParticles[i].run();
  }
}
```

//In this example the execution is still lenear, but the objects within ```arrayOfParticle``` seemingly operates in different threads.

</br></br></br>
____
</br></br></br>

However the true power lies in the interaction across multiple objects, where the evens and messages are not controlled in a linear way.

```JavaScript
//MiniEx7

//particleImpactDistance = windowWidth/5
// This is within
run() {
  this.growAndRenderParticle();
  this.particleWithinRange();
  this.moveParticle();

}
particleWithinRange() {

  for (let i = 0; i < arrayOfParticles.length; i++) { //&& 0 != this.position.sub(arrayOfParticles[i].position
    push();
    strokeWeight(1);
    stroke(50,50,50,10);
    line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
    pop();
    if (this.position.dist(arrayOfParticles[i].position) < particleImpactDistance && 0 != this.position.dist(arrayOfParticles[i].position)) {

      push()
      strokeWeight(3);
      stroke(random(255), random(255), random(255));
      line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
      pop()

      let midPointX = (this.position.x + arrayOfParticles[i].position.x) / 2;
      let midPointY = (this.position.y + arrayOfParticles[i].position.y) / 2;
      if (this.counter > this.particleBirthDelay && !this.gaveBirthToParticle && !arrayOfParticles[i].gaveBirthToParticle) {
        new particle(midPointX, midPointY, false);
        this.counter = 0;
        this.particleBirthDelay = this.particleBirthDelay*this.particleBirthDelay;
        //this.gaveBirthToParticle = true;
      }
    }
  }
  this.counter++;
}
```

![](assets/particles.png)


</br></br></br>
____
</br></br></br>

### <b>The Relation between each objects is therefore reality complex and dynamic, but at the same time it allows for a more flexible relationship between user and machine - especially within the HCI field</b>

</br></br></br>
____
</br></br></br>

//However, interaction with object can only be successfully done in the way that is specified by the object, while the computer more rarely adjust itself according to the user. This suggest asymmetric relationship between the user and the object.

```JavaScript
let mads = {
    "hairColor": "brown",

    "sayHello": function() {
      console.log('hello!');
    },
    "doSomething": function(word) {
      try {
        let result = word.toUpperCase();
        return result;
      }
      catch (err) {
        console.log('you need to parse a string to use this method!!');
      }
    }
  }
````

![](assets/markdown-img-paste-20200606104059537.png)

</br></br></br>
____
</br></br></br>

## They make it easier to reuse code

Class libraries like p5.js also comes with a bunch of different objects containing methods and other properties.

e.g. the ```p5.Element()```

_____
</br></br></br></br>

### Easier to extend than disrupt

</br></br></br>
_____
</br></br></br></br>

### In some cases this causes som implications
```JavaScript
// Example drawn from Modifying the universal by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting
class FaceEmoji {
  constructor(mood, gender, hairstyle, proffession) {
    this.mood = mood;
    this.gender = gender;
    this.hairstyle = hairstyle;
    this.proffession = proffession;
    this.create();
  }
  create() {
    //Code to display emoji
    console.log('Emoji has been drawn');
  }
  doSomething() {
    //Do somthing like "tickling" while mouse hover or animate
  }
}



class FaceEmojiMod extends FaceEmoji {
  constructor(mood, gender, haistyle, proffession, fitzpatrick) {
    super(mood, gender, haistyle, proffession);
    this.fitzpatrick = fitzpatrick;
    this.modifySkinTone();
  }
  modifySkinTone() {
    if (typeof this.fitzpatrick != 'undefined') {
      console.log('Skintone has been modified');
    }
  }
}
```
</br></br></br>

---
</br></br></br>

"The relation of inheritance implies a situation in which objects extend and expand their territory
through small variations, incremental additions that confirm rather than disrupt expectations
about how objects should behave. <b>To put it crudely, it is easier to inherit and extend (to
assume that small differences are deviations from a broadly accepted norm) than it is to consider
that such variations might be indices of a different situation, a different world</b>"

</br></br></br>
____
</br></br></br>
