# Dom-elements - data capturing

---
</br></br></br>

- ### What is a Dom elements
- ### Illusion of mechanical cause-and-effect relationship
  - #### Persuasive
  - #### Simplifying
  - #### Multiple layers
    - ##### Black Box
- ### Network of actions
- ### The "like" button

</br></br></br>

---

</br></br></br>

## DOM: Document Object Model

In languages like HTML/XHTML/XML the DOM is the object oriented representagen of e.g. a webpage. On wepages it can be a canvas, div, image, video, button etc. basically it covers all the individual elements of a HTML page.

It also works as an interface for JavaScript (p5.js) to manipulate and interpret these elements and interactions.

</br></br></br>
_____
</br></br></br>


in p5.js we can access the element in by using the built in function ```elt()```

Example:
```Javascript
EXIF.getData(img.elt, function() {
  console.log(EXIF.getAllTags(this));
  dataLabels = Object.keys(EXIF.getAllTags(this));
  dataValues = Object.values(EXIF.getAllTags(this));
  for(let i = 0; i < dataLabels.length;i++){
      exifData.push([dataLabels[i], dataValues[i]]);
  }
```
//There are of course multiple ways of manipulating these elements in p5.js  


</br></br></br>
_____
</br></br></br>


### Interactive DOM Elements:
  - button
  - slider
  - input box  
  - checkbox
  - etc.

</br></br></br>

---

</br></br></br>

<b>Represent physical elements and therefore there is often the belief that there is a mechanical cause-and-effect relationship</b>
//button
//Tangible

</br></br></br>

---

</br></br></br>
They are persuasive because it gives a sense of control + an easy reward

_____
</br></br></br>

#### We might perceive it as this: button ---> function

 ```Javascript
   let button = createButton('Do Something');
   button.mousePressed(doSomething);

   function doSomething(){
     console.log('It did something!')
   }
 ```
 </br></br></br>
_____
</br></br></br>

In reality it consist of many complex layers.

</br></br></br>
____
</br></br></br>

#### Both technological...
![](assets/markdown-img-paste-20200605103100422.png)

Source: https://medium.com/@twitu/a-dive-down-the-levels-of-abstraction-227c96c7933c

</br></br></br>

---

</br></br></br>
#### ... but also functional + cultural

![](assets/facebook_likeeconomy.png)

 - <b>The seemingly "Direct" connection between button and function</b>
 - <b>The aggregation of all interactions </b>
 - <b>The connection between the liker and the likers profile</b>

</br></br></br>

---

</br></br></br>

<b>"We nevertheless see and interpret it as something that triggers a function—and for good reason, since it is designed to perform in this way. It is a software simulation of a function, and this simulation aims to hide its mediated character and acts as if the function were natural or mechanical in a straight cause- and- effect relation"<b/>

</br></br></br>

---

</br></br></br>

<b> In todays computation buttons and other elements function in a larger network. This is often unnoticeable to the user<b/>


![](assets/Facebooklikebutton.png)

</br></br></br>
_____
</br></br></br>

<b>Implementation of facebook's like button also comes with Facebook Connect, whch connects pages to the Facebook Graph. While not only reading interaction, this implementation of Facebook social plugins also tracks user behavior across multiple sites.</b>

![](assets/Facebooklikebuttonnetwork.png)

</br></br></br>
____
</br></br></br>

<b>This generates all generates economic value and causes all clicks to have multiple relations in a larger netowrk</b>


![](assets/ComButtonVSMechButton.png)

</br></br></br>
_____
