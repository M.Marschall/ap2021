---
layout: "Presentation"
title: "Loops"
date: "2020-06-03 13:53"
---
## Disposition
- ### What is a loop?
- ### Time dertermined Loops
  - #### ```draw()```
  - #### ```setInterval()```
- ### Task determined loops
  - #### ```for(let i = 0; i<10; i++){}```
  - #### ```while(i<10){}```

</br></br></br>

---

</br></br></br>

## Time vs Task determined
<b>Task determined loops are executing as fast as the machine allows where time determined loops have a specified delay between each iteration</b>

---

</br></br></br>

Within computer programming a loop is can repeatedly run code and continue to do so until a condition is met. If that condition is never met it will become an infinite loop.(Soon, )
</br></br></br>
____
</br></br></br>

#### P5.js - ```Draw()```
p5.js is a high-level programming language and includes a standard looping function which will continue to execute every 1/60 unless specified otherwise.
```Javascript
let num = 0;

function draw(){
  num++;
}
```
In this case it will count 1 every iteration. If we would like to terminate this it would be by calling the ```noLoop()```

```Javascript
let num = 0;

function draw(){
  if(num>10){
    noLoop();
  }
  num++;
}

```

In p5.js this snippet will execute the draw function 11 times before it stops.

</br></br></br>
___
</br></br></br>
#### JavaScript - ```setInterval()```

Another time related syntax that also creates a loop is the ```setInterval()``` which is build-in JavaScript function
```Javascript
setInterval(doSomething, 500);

function doSomething(){

}

```

to terminate the ```setInterval()``` JavaScript also comes with a build-in function  ```clearInterval()```

</br></br></br>

---
</br></br></br>
### The throbber blurs the difference between time and task determined Loops

#### Process vs delay?

</br></br></br>

---

</br></br></br>


![](assets/markdown-img-paste-20200603171121242.png)

Throbber has become a cultural symbol for process, interruption of stream, and unproductive wait.

loops are about computational processes and when used in a throbber it also becomes a signifier for process. The throbber is however running independently from the main process of retrieving or loading data. So in reality it is a time determined loop that signifies a computational process including task determined loops, functions etc.  

This relations is also why we have this idea of data being a stream, even though there is not such thing, becuase what we see as a stream is actually not continuous but individual packages of data(soon, ).

### Micro-temporality
---

</br></br></br>


#### Nothing is instantaneous within computation

</br></br></br>


---

</br></br></br>

In computation there a lot of temporal micro events, like one the iteration of a loop in a high level languages like p5.js might cause a bunch of micro events on machine level.

</br></br></br>


---

</br></br></br>


<b>"It manifest itself as an epistemic model of a machine that makes time itself logically controllable and, while operating, produces measurable time effects and rhythms"</b>

</br></br></br>

---

</br></br></br>


Execution time will always be dependent on a physical process of sending, receiving, storing and processing of physical signals(http://computationalculture.net/algorhythmics-understanding-micro-temporality-in-computational-cultures/)

</br></br></br>

____
</br></br></br>

### These processes are inaccessible by human senses and the author uses the term trans-sonic describes all these process that are inaudible to humans.

### The throbber hides these trans-sonic signals.
</br></br></br>
____
</br></br></br>


#### Timing microprocesses in networks
//'Streaming' is an illusion


Timing is crucial when it comes to exchanging packages over TCP/IP(Transmission Control Protocol/Internet Protocol).

//Tid er relativt i programmering fordi at det handler om en process og ikke sekunder, minutter osv.
