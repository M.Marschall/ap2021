___
___
___
# Vocable Code + Language
---
---
---

____

</br></br></br>

### Natural language vs programming language

 - #### they both have a specific syntax that must be followed
 - #### the grammar of the language is preprogrammed (Chomsky 1957)

 </br></br></br>

___
</br></br></br>

### High-level programming languages like JavaScript (p5.js) are typically closer to natural languages

```JavaScript
//p5.js
createCanvas(400,400)

//JavaScript
let ball = new Object();
```


![](assets/languageLevel.jpeg)
https://www.quora.com/What-are-examples-of-high-level-languages

</br></br></br>
---
</br></br></br>



</br></br></br>
---
</br></br></br>


 - #### Layers of abstraction
 - #### Micro Decisions
 - #### Do we know what is going on?

 </br></br></br>
___
</br></br></br>


 #### file.madsCode

 ```
 lav_en_ny_funktion sigHej() {
   lad hilsen = 'hej med dig';

   gentag(lad i = 0; i<10; i++) {
     skrivTilKonsol(hilsen);
   }
 }

 sigHej();

 ```
 </br></br></br>

 ---
 </br></br></br>

 #### sketch.js

 ```JavaScript
 function setup(){
   $.ajax({
       url : "file.madsCode",
       dataType: "text",
       success : function (data) {
           data = madsCodeCorrector(data);
           window.eval(data);
       }
   });

 }



 function madsCodeCorrector(kode){

   let skrivTilKonsol = kode.split('skrivTilKonsol').length - 1
   for(let i = 0; i< skrivTilKonsol;i++){
     kode = kode.replace('skrivTilKonsol','console.log');
   }
   let lad = kode.split('lad').length - 1
   for(let i = 0; i< lad;i++){
     kode = kode.replace('lad','let');
   }

   let gentag = kode.split('gentag').length - 1
   for(let i = 0; i< gentag;i++){
     kode = kode.replace('gentag', 'for')
   }

   let lav_en_ny_funktion = kode.split('lav_en_ny_funktion').length - 1
   for(let i = 0; i< lav_en_ny_funktion;i++){
     kode = kode.replace('lav_en_ny_funktion', 'function');
   }
   return kode;
 }

 ```
 </br></br></br>

 ---
 </br></br></br>

 ![](assets/languageExecutingMadsCode.png)

 </br></br></br>

 ---
 </br></br></br>
<b>

  Because of the layers of abstraction, code has qualities beyond execution.

 High-level code is abstract signfiers of an underlying process, bur this secondary notation is equally important. This is also somewhat similiar to natural language where we also use signifiers.

 </b>
 </br></br></br>

---
</br></br></br>

 ## Tt is a language and have the properties of a language:

 </br></br></br>
 ____
 </br></br></br>

### instability


<b> "it is only really interpretable within the context of the overall network of relations that make its operations inherently unstable"</b>

(Geoff Cox, and Alex McLean. Speaking Code. Cambridge, Mass.: MIT Press, 2013)

---

```JavaScript
function doesHeLoveHim(){
  let words = ' He told him that he loved him ';
  let withDoubt = true;
  if(withDoubt){
    let uncertainty = words.split(' ');
    let decision = round(random(0,uncertainty.length - 1));
    uncertainty.splice(decision, 0,"only")+'';

    words = '';

    for(let word = 0; word < uncertainty.length; word++) {
      words += uncertainty[word]+' ';
    }
    return words;
  }
  else {
    withDoubt = false; //Psuedocode
    return words;
  }
}


```

![](assets/markdown-img-paste-20200608121242239.png)
____
</br></br></br>
  - ### Fragile
  - ### Obselete syntax and expressions

</br></br></br>
____

</br></br></br>
### //Pseudocode
- #### Non-executable code or code that dose not do anything in general.

</br></br></br>

---
</br></br></br>


 #### "computer programs have bodies in the sense that other materialities and meanings are deeply interwoven, and these necessarily exist as part of wider social relations" Vocable Code

  - Apart of the execution (e.g. Machine Learning)
  - Computational Thinking (abstracting and executing the world)
  - Execution trough speech

  </br></br></br>

---
</br></br></br>

## Speechlike quality of code

#### In programming, the language can be compared to speech acts – meaning that there is both a performative aspect (execution) it and operative aspect (indication of execution).  

#### Like ```loadJSON(url,callback)``` is both an act and indication of an act

</br></br></br>
___
</br></br></br>

#### Executable code can be seen as an illocutionary act because it executes in the process of saying something

#### illocutionary act -  can be captured by emphasizing that "by saying something, we do something"

</br></br></br>
___
</br></br></br>

#### Psuedo and executable can however both carry an Perlocutionary Effect

#### Computation in general
- ##### how we structure an if()
- ##### true vs false
- ##### etc.

</br></br></br>
____
</br></br></br>


Vocalble Code by Winnie Soon

</br></br></br>
___
