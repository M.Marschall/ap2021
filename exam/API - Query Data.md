___
---
___

# API - Query data + JSON

___
___
___

</br></br></br>


### JSON
- #### Interoperability of JSON
- #### Enables sharing of data between programs

- #### JSON = “name:value” pairs
- #### Has its own grammar

//_JSON downloaded from facebook_

Mads Marschall:
```JSON
{
  "friend_peer_group": "Starting Adult Life"
}
```

Kenneth Marschall:
```JSON
{
  "friend_peer_group": "Etableret voksenliv"
}
```
</br></br></br>

___
</br></br></br>


### API - Application Programming Interface
- #### Web API
- #### Enables exchanging of data across multiple platforms

</br></br></br>
![](assets/markdown-img-paste-20200609010034360.png)
http://www.gurujipoint.com/2017/10/what-is-web-api-and-why-do-we-need-web.html
</br></br></br>

___
</br></br></br>

### Query = "a question, especially one expressing doubt or requesting information" Oxford Dictionary
 - #### Query Parameters

```JavaScript
function fetchImage() {
	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize  + "&searchType=" + searchType + "&q=" + query;
	console.log(request);
	loadJSON(request, gotData); //this is the key syntax and line of code to make a query request and get a query response
}
```
by Winnie Soon:https://editor.p5js.org/siusoon/sketches/wmJd5awLJ
</br>
<b> HTTPS protocol allows for requesting and receiving responses from a server in the for of a URL containing different parameters.

</br></br></br>
___
</br></br></br>






#### A simple API request:
```JavaScript
//MiniEx9
loadJSON('https://api.adviceslip.com/advice',doSomethingWithQuote);

```

</br></br></br>
___
</br></br></br>

## Asymmetrical power relations


</br></br></br>
___

</br></br></br>


#### We are dependent on APIs for the software to function and we are in the risk of rejection

![](assets/markdown-img-paste-20200609011921198.png)

</br></br></br>
---
</br></br></br>



#### This risk-zone establish asymmetric relation from point of handshake

![](assets/markdown-img-paste-20200609011610423.png)



</br></br></br>

___
</br></br></br>


#### On Request
- Parameters required
  - e.q. Token
  - e.q. Id
  - Ofther authentication types


</br></br></br>

---
</br></br></br>


#### Profiling and tracking behavior


</br></br></br>

---
</br></br></br>


### Blackbox processes determine data within response
![](assets/dependentOnAPI.png)



</br></br></br>

---
</br></br></br>




### Algorithmic Governmentality
![](assets/markdown-img-paste-20200609015716775.png)


</br></br></br>
____

## Question normalization of corporate API practices
  - ### e.g.
    - ### blackbox algorithms
    - ### Protocological Parameters
    - ### Economics
