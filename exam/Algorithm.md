# Algorithms

___

</br></br></br>

___
</br></br></br>


## Algorithm = input => output
##### a process or set of rules to be followed in calculations or other problem-solving operations, especially by a computer(oxford dictionary)
 - a recipe for how to do an action

```JavaScript
function halfOfNumber(input) {
  let output = input/2;
  return output;
}
```



</br></br></br>
____
</br></br></br>

## Algorithms are usually deterministic themselves
//(_maybe except for reinforcement algorithms within machine learning_)

### The same input will map to the same output
### + They will always produce an output


</br></br></br>
___
</br></br></br>

## Politics are embedded in algorithms
 - ### The data process
```JavaScript
classifier.classify(captureFlipped, handleGender);
```
  - #### e.g. The conditional structure
  ```JavaScript
if (gender.localeCompare("Female") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    for (let t = 0; t < jobs[0].jobsforwomen.length; t++) {//Goin through element in array
      let posY;
      posY = 50 * t + 400;//Increments the y position of the text
      text(jobs[0].jobsforwomen[t], 1100, posY);//Display the job
    }
  }
if (gender.localeCompare("Male") == 0) {//gender.localeCompare() returns 0 if the argument is the same value as the string variable
    for (let t = 0; t < jobs[0].jobsformen.length; t++) {//Going through each element in array
      let posY;
      posY = 50 * t + 400;//Increments the y position of the text
      text(jobs[0].jobsformen[t], 1100, posY);//Display the job
    }
}
  ```

___
</br></br></br>

  ### It's placement
  </br></br></br>

____
</br></br></br>

## Algorithms operate in a complex network of actions upon actions
### - facerec.js

```JavaScript
//......
function classifyGender() {
  captureFlipped = ml5.flipImage(capture); //Flipping the images input from webcam
  classifier.classify(captureFlipped, handleGender);//Classifing the captureFlipped according to our model and call handleGender and passing the result or error as an argument
  captureFlipped.remove();//Remove the stored image to allow for another image to be assigned
}

function handleGender(error, results) {
  if (error) {
    console.error(error);//if anything is assign to the error argument then display it in the console
    return;
  }


  label = results[0].label //The "result" argument parsed from classifier.classify() is an array with the most proable label at index 0
  arrayOfResults.push(label);//We add this label to an array
  console.log(label);//prints the label to the console
  counter++;
  if (counter > 10) {
    clearInterval(interval);//Stops the interval from looping
    userGender = determineGender();//assigning the reurned value from determineGender();
  }
}
//........
```
----

//This drawing only includes some of the entities involved
![](assets/markdown-img-paste-20200609135708304.png)
// i do not no why there is only one developer....

</br></br></br>
____
</br></br></br>

## Algorithms are usually deterministic themselves
//maybe except for reinforcement algorithms

### The same input will map to the same output

### However, The effects and relations are non deterministic
 - ### The network in which they operate diffuse the correlation between inputs and outputs


 </br></br></br>
____
</br></br></br>

## Govermentality of algorithms
### conducting the conduct

</br></br></br>
___
</br></br></br>

## System wise
![](assets/markdown-img-paste-20200609141540896.png)

![](assets/markdown-img-paste-20200609131243251.png)

</br></br></br>
____
</br></br></br>

## Socially and culturally
![](assets/markdown-img-paste-20200609140257155.png)

____

![](assets/markdown-img-paste-20200609143011328.png)
