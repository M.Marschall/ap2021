____
____
___

# Geometry and shapes
___
___
___
</br></br></br>

![](assets/markdown-img-paste-20200609113606571.png)
https://p5js.org/examples/form-regular-polygon.html


   - #### Shapes and figures
     - Circle ```circle()```
     - ellipse ```ellipse()```
     - Triangle ```triangle()```
     - Rectangle ```rect()```
     - Square ```square()```
     - line ```line()```
     - Polygon ```beginShape(); endShape()```
     - etc
   - #### Visual Abstraction
     - Ways of thinking
     - Abstraction of reality
     - Communication
     - Emojies and Representation

     </br></br></br>

____

</br></br></br>

Emoji in its simplest form:
<font size="+70">:)</font>

</br></br></br>
___

## _Multi by David Reinfurt_
![](assets/00004.gif)

---

## Modifying the universal

![](assets/markdown-img-paste-20200602123724652.png)
https://notebook.hongkonggong.com/2017/10/12/inclusive-skintones/


#### Become representation of identity, where the user selects from a set of predefined faces and skin color etc.

#### Overly abstracted and superficial version of ones identity, and forcing the question of skin color into everyday life.
____
</br></br></br>

```JavaScript
// Example drawn from Modifying the universal by Roel Roscam Abbing, Peggy Pierrot and Femke Snelting
class FaceEmoji {
  constructor(mood, gender, hairstyle, proffession) {
    this.mood = mood;
    this.gender = gender;
    this.hairstyle = hairstyle;
    this.proffession = proffession;
    this.create();
  }
  create() {
    //Code to display emoji
    console.log('Emoji has been drawn');
  }
  doSomething() {
    //Do somthing like "tickling" while mouse hover or animate
  }
}



class FaceEmojiMod extends FaceEmoji {
  constructor(mood, gender, haistyle, proffession, fitzpatrick) {
    super(mood, gender, haistyle, proffession);
    this.fitzpatrick = fitzpatrick;
    this.modifySkinTone();
  }
  modifySkinTone() {
    if (typeof this.fitzpatrick != 'undefined') {
      console.log('Skintone has been modified');
    }
  }
}
```


### Possible issues:

  - #### The releationship between the baseline emoji and its modifiers, is problematic since these relations have been created subjectively by the industry.
  - #### To state diversity as a variant is too reductive
  - #### the modyfiers themself are always questionable - like hair and skin tone modifier.

  </br></br></br>
___
</br></br></br>

### Blackfacing?

</br></br></br>

---
</br></br></br>



### Increasing resolution is not a solution.


</br></br></br>

---

</br></br></br>

#### We need to question "the assumption that everything can and should be encoded into the same system"(Abbing, Pierrot, and Snelting 2017).

</br></br></br>
___
</br></br></br>


## Other Geometries
```Javascript
//"The term “circle” can refer to the outline of a figure,or to a round shape, including its interior"(Snelting, 2019)
circle(30, 30, 20);
```
![](assets/markdown-img-paste-20200602104111396.png)

---

<b>"The obligation to always stay at the same distance from the center promises a situation of equality but does so by conflating it with similarity. Circles divide spaces into an interior and ane xterior, a binary separation that is never easy to overcome"</b>(Snelting, 2019)

</br></br></br>


---
</br></br></br>

### The circle is appropriating identity to fall into the same diameters

</br></br></br>

___
</br></br></br>


<b>"...to imagine togetherness with difference, we first need to change our frames of reference to ones that   <font size="+2">do not depend on zero eccentricity</font>
. This shifting of geometries is a necessary step to come up with technological renderings of possible non-utopian models that go beyond the rigidifying assumptions of sameness and reciprocity"</b>(Snelting, 2019)

</br></br></br>
___
</br></br></br>


```Javascript
beginShape();
vertex(40, 40);
vertex(90, 40);
vertex(200, 170);
vertex(168, 80);
vertex(210, 40);
vertex(40, 10);
endShape(CLOSE);
  ```
![](assets/markdown-img-paste-20200602105447510.png)


</br></br></br>

---
</br></br></br>

##  We need to strengthen equality by embracing differences, not by appropriating them.

</br></br></br>

---
