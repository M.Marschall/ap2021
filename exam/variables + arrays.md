# variables + arrays
---
</br></br></br>

### Variables are used to store values and its name is a signifier of that value.

</br></br></br>
___
</br></br></br>

#### Local variables can only be accessed in a particular block of code

#### Global variables can be accessed from everywhere

</br>
</br>
</br>

____

</br>
</br>
</br>

```JavaScript
function letHimOut() {
  let freedom = 'set free';
  let him = new Number(9).toFixed()
  let escape = true;
  if(escape){
    window.him = him;
    return freedom;
  }
}

function findHim() {
  try {
    return him;
  }
  catch(failure){
    console.log(failure)
    console.log('He could not be found');
  }
}
```
![](assets/markdown-img-paste-20200609120346734.png)
</br>
</br>
</br>

---

</br></br></br>

### Variables are dynamic and can always be changed
![](assets/markdown-img-paste-20200609120723614.png)

</br></br></br>
____
</br></br></br>

## Arrays

</br></br></br>
____
</br></br></br>

### Arrays store a list of values
```JavaScript
let greetings = ["what's up!", "hello!", "welcome"]
```
</br></br></br>
____
</br></br></br>

### Arrays start at index 0
```JavaScript
let greetings = ["what's up!", "hello!", "welcome"]
console.log(greetings[0])//"what's up!"
console.log(greetings[1])//"hello!"
console.log(greetings[2])//"welcome"
````
</br></br></br>
____
</br></br></br>

### like any other arrays has its own object wrapper that contains methods for manipulation
```JavaScript
let greetings = ["what's up!", "hello!", "welcome"]
console.log(greetings.length)// 3
greetings.push("hola!");
console.log(greetings.length)// 4

greetings.shift();
console.log(greetings.length)// 3
```

</br></br></br>
___

# //Continue on language
