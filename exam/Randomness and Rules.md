# Auto Generator and Randomness

----
</br></br></br>

## Pseudo randomness vs True Randomness

- #### PRNGs(Psuedo Random Number Generators) generated numbers trough computation - emulating Randomness

- #### TRNGs(True Random Number Generators) generates random number based on physical randomness like thermalnoise, Quantum fluctuation, etc.

</br></br></br>

---

</br></br></br>

Randomness has multiple uses in programming
- Experimental music
- Generative Art
- Security and Encryptions
- Games  
- etc

</br></br></br>

---
</br></br></br>

#### In an extremely long sequence will eventually generate the same number many times in a row. Therefore we use Pseudo randomness.

</br></br></br>

---

</br></br></br>

Different systems(e.g. browsers) have different RNGs and they are continually being developed.

```JavaScript
Math.random()
```
which is the one utilized in the p5.js function:
```JavaScript
random(min,max);
```
Deterministic process that relies on the state of the computer, where the process run through multiple levels of abstractions.

</br></br></br>
____
</br></br></br>
### The bad PRNGs - RANDU by IBM
```JavaScript
//https://www.infragistics.com/community/blogs/b/tim_brock/posts/foundations-of-random-number-generation-in-javascript

//Inspired by Tim Brock's JavaScript RANDU
class Randu {
  constructor(seed){
    if(!isFinite(seed)){
       throw new Error("Seed not a finite number");
    }

    let x = Math.round(seed);
    let a = 65539;
    let m = Math.pow(2, 31);//2147483648

    if(x<1 || x>=m){
       throw new Error("Seed out of bounds");
    }
    return function(){
       x = (a*x)%m;
       return x/m;
    };
  }
};
```

---

<b> 512*512 images with values applied "randomly"</b>


![](assets/markdown-img-paste-20200607131822307.png)

![](assets/markdown-img-paste-20200607131838308.png)

---

##### seed of Randu = 16384 = 128*128



![](assets/markdown-img-paste-20200607131918227.png)

![](assets/markdown-img-paste-20200607132110420.png)
---

</br></br></br>

#### Point = In theory statistics behind Pseudo Randomness can be unmasked

</br></br></br>
---
</br></br></br>

#### Security, Encryptions, Tokens, SSL, Simulations, etc.


</br></br></br>
---
</br></br></br>

#### Randomness and creativity

![](assets/markdown-img-paste-20200607140445471.png)

---
</br></br></br></br>

<b>The Artist Becomes the producer</b>


<b>Source of inspiration</b>

</br></br></br>
_____
</br></br></br>
## Rules
if(random) {
  <b>

  - randomness is executed within a given set of rules by the programmer, therefore it can be said that nothing is truly random.
  - Our Society is build upon rules and this is also evident in programming
  </b>

}

</br></br></br>
____
</br></br></br>

### Programming allows for not just following rules but creating them

</br></br></br>
_____
</br></br></br>

```JavaScript
function draw() {
  stroke(255);
  if (random(1) <0.5) {  //probabilty
    //line(0,0,10,10); //backward slash example
    line(x,y,x+spacing,y+spacing);
  } else {
    //line(0,10,10,0); //forward slash example
    line(x,y+spacing,x+spacing,y); //forward slash
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;

  }
}

```
by Winnie Soon

</br></br></br>
____
</br></br></br>

### Simulating rules of the world

//Snippet from biods simulation by Craig Reynolds
```JavaScript
// Separation
// Method checks for nearby boids and steers away
Boid.prototype.separate = function(boids) {
  let desiredseparation = 25.0;
  let steer = createVector(0, 0);
  let count = 0;
  // For every boid in the system, check if it's too close
  for (let i = 0; i < boids.length; i++) {
    let d = p5.Vector.dist(this.position,boids[i].position);
    // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
    if ((d > 0) && (d < desiredseparation)) {
      // Calculate vector pointing away from neighbor
      let diff = p5.Vector.sub(this.position, boids[i].position);
      diff.normalize();
      diff.div(d);        // Weight by distance
      steer.add(diff);
      count++;            // Keep track of how many
    }
  }
```
</br></br></br>

____

</br></br></br>


### Systems of rules and patterns are present all around us, and we can try to replicate that

</br></br></br>

____

</br></br></br>

### Simulate rules of other worlds
//Beautiful rules: Generative models of creativity - Marius Watz
// proof that computers are not necessarily trying to emulate objects of the world

</br></br></br>
_____

</br></br></br>

### Rules allows for making the system external and self operative
### Psuedo randomness allows for never repeating patterns

</br></br></br>
